<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="README_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CLib<sp />FreeRTOS<sp />Support</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />CLib<sp />FreeRTOS<sp />Support<sp />Library<sp />provides<sp />the<sp />necessary<sp />hooks<sp />to<sp />make<sp />C<sp />library<sp />functions<sp />such<sp />as<sp />malloc<sp />and<sp />free<sp />thread<sp />safe.<sp />This<sp />implementation<sp />is<sp />specific<sp />to<sp />FreeRTOS<sp />and<sp />requires<sp />it<sp />to<sp />be<sp />present<sp />to<sp />build.<sp />For<sp />details<sp />on<sp />what<sp />this<sp />library<sp />provides<sp />see<sp />the<sp />toolchain<sp />specific<sp />documentation<sp />at:</highlight></codeline>
<codeline><highlight class="normal">*<sp />ARM:<sp />https://developer.arm.com/docs/100073/0614/the-arm-c-and-c-libraries/multithreaded-support-in-arm-c-libraries/management-of-locks-in-multithreaded-applications</highlight></codeline>
<codeline><highlight class="normal">*<sp />GCC:<sp />https://sourceware.org/newlib/libc.html#g_t_005f_005fmalloc_005flock</highlight></codeline>
<codeline><highlight class="normal">*<sp />IAR:<sp />http://supp.iar.com/filespublic/updinfo/011261/arm/doc/EWARM_DevelopmentGuide.ENU.pdf</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />startup<sp />code<sp />must<sp />call<sp />cy_toolchain_init<sp />(for<sp />GCC<sp />and<sp />IAR).<sp />This<sp />must<sp />occur<sp />after<sp />static<sp />data<sp />initialization<sp />and<sp />before<sp />static<sp />constructors.<sp />This<sp />is<sp />done<sp />automatically<sp />for<sp />PSoC<sp />devices.<sp />See<sp />the<sp />PSoC<sp />startup<sp />files<sp />for<sp />an<sp />example.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">To<sp />enable<sp />Thread<sp />Local<sp />Storage,<sp />configUSE_NEWLIB_REENTRANT<sp />must<sp />be<sp />enabled.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">While<sp />this<sp />is<sp />specific<sp />to<sp />FreeRTOS,<sp />it<sp />can<sp />be<sp />used<sp />as<sp />a<sp />basis<sp />for<sp />supporting<sp />other<sp />RTOSes<sp />as<sp />well.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Requirements</highlight></codeline>
<codeline><highlight class="normal">To<sp />use<sp />this<sp />library,<sp />the<sp />following<sp />configuration<sp />options<sp />must<sp />be<sp />enabled<sp />in<sp />FreeRTOSConfig.h:</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_MUTEXES</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_RECURSIVE_MUTEXES</highlight></codeline>
<codeline><highlight class="normal">*<sp />configSUPPORT_STATIC_ALLOCATION</highlight></codeline>
<codeline />
<codeline><highlight class="normal">When<sp />building<sp />with<sp />IAR,<sp />the<sp />'--threaded_lib'<sp />argument<sp />must<sp />also<sp />be<sp />provided<sp />when<sp />linking.<sp />This<sp />is<sp />done<sp />automatically<sp />with<sp />psoc6make<sp />1.3.1<sp />and<sp />later.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Features</highlight></codeline>
<codeline><highlight class="normal">*<sp />GCC<sp />Newlib<sp />implementations<sp />for:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_sbrk</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__malloc_lock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__malloc_unlock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__env_lock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__env_unlock</highlight></codeline>
<codeline><highlight class="normal">*<sp />ARM<sp />C<sp />library<sp />implementations<sp />for:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_platform_post_stackheap_init</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__user_perthread_libspace</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_mutex_initialize</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_mutex_acquire</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_mutex_release</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_mutex_free</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_sys_exit</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />$Sub$$_sys_open</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_ttywrch</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_sys_command_string</highlight></codeline>
<codeline><highlight class="normal">*<sp />IAR<sp />C<sp />library<sp />implementations<sp />for:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__aeabi_read_tp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />_reclaim_reent</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_system_Mtxinit</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_system_Mtxlock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_system_Mtxunlock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_system_Mtxdst</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_file_Mtxinit</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_file_Mtxlock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_file_Mtxunlock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_file_Mtxdst</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_Initdynamiclock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_Lockdynamiclock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_Unlockdynamiclock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__iar_Dstdynamiclock</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__close</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__lseek</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />remove</highlight></codeline>
<codeline><highlight class="normal">*<sp />C++<sp />implementations<sp />for:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__cxa_guard_acquire</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__cxa_guard_abort</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />__cxa_guard_release</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />More<sp />information</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2020.</highlight></codeline>
    </programlisting>
    <location file="clib_support/README.doxygen.md" />
  </compounddef>
</doxygen>