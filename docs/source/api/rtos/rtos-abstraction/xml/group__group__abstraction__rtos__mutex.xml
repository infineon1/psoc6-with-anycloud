<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__abstraction__rtos__mutex" kind="group">
    <compoundname>group_abstraction_rtos_mutex</compoundname>
    <title>Mutex</title>
      <sectiondef kind="user-defined">
      <memberdef id="group__group__abstraction__rtos__mutex_1ga79fbca5c574d2bc617af90d4bff6c902" kind="define" prot="public" static="no">
        <name>cy_rtos_init_mutex</name>
        <param><defname>mutex</defname></param>
        <initializer><ref kindref="member" refid="group__group__abstraction__rtos__mutex_1ga0c9c20028f40d04bd4cafdddb2d2e4ca">cy_rtos_init_mutex2</ref>(mutex, true)</initializer>
        <briefdescription>
<para>Create a recursive mutex. </para>        </briefdescription>
        <detaileddescription>
<para>Creates a binary mutex which can be used to synchronize between threads and between threads and ISRs. Created mutexes are recursive and support priority inheritance.</para><para>This function has been replaced by <ref kindref="member" refid="group__group__abstraction__rtos__mutex_1ga0c9c20028f40d04bd4cafdddb2d2e4ca">cy_rtos_init_mutex2</ref> which allow for specifying whether or not the mutex supports recursion or not.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">mutex</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the mutex handle to be initialized</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of mutex creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" bodystart="297" column="9" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="297" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__mutex_1ga0c9c20028f40d04bd4cafdddb2d2e4ca" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_init_mutex2</definition>
        <argsstring>(cy_mutex_t *mutex, bool recursive)</argsstring>
        <name>cy_rtos_init_mutex2</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga20623d7ca01f21ec5ef09879eb2eb166">cy_mutex_t</ref> *</type>
          <declname>mutex</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>recursive</declname>
        </param>
        <briefdescription>
<para>Create a mutex which can support recursion or not. </para>        </briefdescription>
        <detaileddescription>
<para>Creates a binary mutex which can be used to synchronize between threads and between threads and ISRs. Created mutexes can support priority inheritance if recursive.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Not all RTOS implementations support non-recursive mutexes. In this case a recursive mutex will be created.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">mutex</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the mutex handle to be initialized </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">recursive</parametername>
</parameternamelist>
<parameterdescription>
<para>Should the created mutex support recursion or not</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of mutex creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="312" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__mutex_1gaad8f978c0a31817f870f193ec72eb5b2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_get_mutex</definition>
        <argsstring>(cy_mutex_t *mutex, cy_time_t timeout_ms)</argsstring>
        <name>cy_rtos_get_mutex</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga20623d7ca01f21ec5ef09879eb2eb166">cy_mutex_t</ref> *</type>
          <declname>mutex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga141349b315f2266d5e0e8b8566eb608b">cy_time_t</ref></type>
          <declname>timeout_ms</declname>
        </param>
        <briefdescription>
<para>Get a mutex. </para>        </briefdescription>
        <detaileddescription>
<para>If the mutex is available, it is acquired and this function returned. If the mutex is not available, the thread waits until the mutex is available or until the timeout occurs.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function must not be called from an interrupt context as it may block.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">mutex</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the mutex handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">timeout_ms</parametername>
</parameternamelist>
<parameterdescription>
<para>Maximum number of milliseconds to wait while attempting to get the mutex. Use the <ref kindref="member" refid="group__group__abstraction__rtos__common_1gab31d8535128df9506a4220c2b1f41a60">CY_RTOS_NEVER_TIMEOUT</ref> constant to wait forever. Must be zero if in_isr is true.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the get mutex. Returns timeout if mutex was not acquired before timeout_ms period. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1gafc6a1c6f563521d6c5555027d5d7bde8">CY_RTOS_TIMEOUT</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="330" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__mutex_1ga7b6a9f5fc3910436675c00130399e06d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_set_mutex</definition>
        <argsstring>(cy_mutex_t *mutex)</argsstring>
        <name>cy_rtos_set_mutex</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga20623d7ca01f21ec5ef09879eb2eb166">cy_mutex_t</ref> *</type>
          <declname>mutex</declname>
        </param>
        <briefdescription>
<para>Set a mutex. </para>        </briefdescription>
        <detaileddescription>
<para>The mutex is released allowing any other threads waiting on the mutex to obtain the semaphore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">mutex</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the mutex handle</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the set mutex request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="342" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__mutex_1ga8cd21b8a116a54ac397209054f35ee45" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_deinit_mutex</definition>
        <argsstring>(cy_mutex_t *mutex)</argsstring>
        <name>cy_rtos_deinit_mutex</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga20623d7ca01f21ec5ef09879eb2eb166">cy_mutex_t</ref> *</type>
          <declname>mutex</declname>
        </param>
        <briefdescription>
<para>Deletes a mutex. </para>        </briefdescription>
        <detaileddescription>
<para>This function frees the resources associated with a sempahore.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">mutex</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the mutex handle</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status to the delete request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="352" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>APIs for acquiring and working with Mutexes. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>