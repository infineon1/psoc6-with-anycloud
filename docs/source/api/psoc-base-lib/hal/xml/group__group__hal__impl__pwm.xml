<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__impl__pwm" kind="group">
    <compoundname>group_hal_impl_pwm</compoundname>
    <title>PWM (Pulse Width Modulator)</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>

<para><heading level="1">Complementary PWM output</heading></para>
<para>The PWM HAL driver allows generation of a normal and an inverted output. PSoC devices support complementary pin pairs to which the normal and inverted signals can be routed. To identify the complementary pin for a given pin, open the PSoC device datasheet and navigate to the 'Multiple Alternate Functions' table. Each column represents an alternate function of the pin in the corresponding row. Find your pin and make a note of the tcpwm[X].line[Y]:Z. The complementary pin is found by looking up the pin against tcpwm[X].line_<bold>compl</bold>[Y]:Z from the same column. For example, the image below shows a pair of complementary pins (P0.0 and P0.1) identified by the tcpwm[0].line[0]:0 and tcpwm[0].line_compl[0]:0 mapping. These complementary pins can be supplied to <ref kindref="member" refid="group__group__hal__pwm_1gacbeddfe75ccc849d27bec32ce010eceb">cyhal_pwm_init_adv</ref> using <bold>pin</bold> and <bold>compl_pin</bold> parameters in any order. <image name="pwm_compl_pins.png" type="html">Complementary PWM pins</image>
 </para>

<para><heading level="1">PWM Resolution</heading></para>
<para>On PSoC 6 devices, not all PWMs hardware blocks are of the same resolution. The resolution of the PWM associated with a given pin is specified by the TCPWM&lt;idx&gt;_CNT_CNT_WIDTH macro (provided by cy_device_headers.h in mtb-pdl-cat1), where &lt;idx&gt; is the index associated with the tcpwm portion of the entry in the pin function table. For example, if the pin function is tcpwm[1].line[3]:Z, &lt;idx&gt; would be 1.</para><para>By default, the PWM HAL driver will configure the input clock frequency such that all PWM instances are able to provide the same maximum period regardless of the underlying resolution, but period and duty cycle can be specified with reduced granularity on lower-resolution PWM instances. If an application is more sensitive to PWM precision than maximum period, or if a longer maximum period is required (with correspondingly reduced precision), it is possible to override the default clock by passing a <ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref> instance to the <ref kindref="member" refid="group__group__hal__pwm_1ga54a93c541bdbb3fce066a2af00d8b67f">cyhal_pwm_init</ref> function with a custom frequency specified. See the <ref kindref="compound" refid="group__group__hal__clock">Clock</ref> HAL documentation for more details. </para>
    </detaileddescription>
  </compounddef>
</doxygen>