===================
Quadrature Decoder
===================

.. doxygengroup:: group_hal_quaddec
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__quaddec.rst
   

