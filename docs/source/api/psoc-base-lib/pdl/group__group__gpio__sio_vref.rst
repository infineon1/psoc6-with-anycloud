=================================================
SIO reference voltage for input buffer trip-point
=================================================

.. doxygengroup:: group_gpio_sioVref
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: