===============
Command Macros
===============

.. doxygengroup:: group_smif_macros_cmd
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: