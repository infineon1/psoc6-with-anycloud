=================
Enumerated Types
=================


.. doxygengroup:: group_ctdac_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: