<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctb__functions__sample__hold" kind="group">
    <compoundname>group_ctb_functions_sample_hold</compoundname>
    <title>Sample and Hold Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__sample__hold_1ga08a0df606bea1204aa32ea5d343ac650" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTB_DACSampleAndHold</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_sample_hold_mode_t mode)</argsstring>
        <name>Cy_CTB_DACSampleAndHold</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga3f70d00ef0ce087244399f0784065502">cy_en_ctb_sample_hold_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>Perform sampling and holding of the CTDAC output. </para>        </briefdescription>
        <detaileddescription>
<para>To perform a sample or a hold, a preparation step must first be executed to open the required switches. Because of this, each sample or hold requires three function calls:</para><para><orderedlist>
<listitem><para>Call this function to prepare for a sample or hold</para></listitem><listitem><para>Enable or disable the CTDAC output</para></listitem><listitem><para>Call this function again to perform a sample or hold</para></listitem></orderedlist>
</para><para>It takes 10 us to perform a sample of the CTDAC output to provide time for the capacitor to settle to the new value.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Mode to prepare or perform a sample or hold, or disable the ability</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Opamp0<sp />has<sp />been<sp />configured<sp />as<sp />a<sp />buffer<sp />for<sp />the<sp />CTDAC<sp />output</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />with<sp />the<sp />sample<sp />and<sp />hold<sp />(S/H)<sp />capacitor<sp />connected.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />This<sp />code<sp />samples<sp />the<sp />CTDAC<sp />output<sp />once.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Prepare<sp />for<sp />sample.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__sample__hold_1ga08a0df606bea1204aa32ea5d343ac650">Cy_CTB_DACSampleAndHold</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga3f70d00ef0ce087244399f0784065502a2f976a4311b8408fd3b0b4ff7f77bf86">CY_CTB_SH_PREPARE_SAMPLE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Turn<sp />on<sp />DAC<sp />output.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1gaf11a41cf842064d44f9f8cdaeea0a31e">Cy_CTDAC_SetOutputMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccbaa0eedb42bfda13365d835dcc21ef33e6">CY_CTDAC_OUTPUT_VALUE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Sample<sp />DAC<sp />output.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__sample__hold_1ga08a0df606bea1204aa32ea5d343ac650">Cy_CTB_DACSampleAndHold</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga3f70d00ef0ce087244399f0784065502a9f91f672b83cefaa830632f026dc77c4">CY_CTB_SH_SAMPLE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allow<sp />time<sp />for<sp />voltage<sp />to<sp />settle<sp />across<sp />the<sp />hold<sp />capacitor.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syslib__functions_1ga9d2ffe4829c636ad78eaae5043fd6ae6">Cy_SysLib_DelayUs</ref>(10);</highlight></codeline>
</programlisting> <simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Turn<sp />off<sp />the<sp />CTDAC<sp />output<sp />to<sp />save<sp />power<sp />and</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />hold<sp />the<sp />output<sp />voltage<sp />across<sp />the<sp />S/H<sp />capcitor.<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />The<sp />DAC<sp />output<sp />needs<sp />to<sp />be<sp />turned<sp />on<sp />and<sp />sampled<sp />periodically.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />See<sp />the<sp />device<sp />datasheet<sp />for<sp />the<sp />hold<sp />duration.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Prepare<sp />for<sp />hold.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__sample__hold_1ga08a0df606bea1204aa32ea5d343ac650">Cy_CTB_DACSampleAndHold</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga3f70d00ef0ce087244399f0784065502a47aff94f55efbf7a105f71348fdaf2a1">CY_CTB_SH_PREPARE_HOLD</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Turn<sp />off<sp />DAC<sp />output.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1gaf11a41cf842064d44f9f8cdaeea0a31e">Cy_CTDAC_SetOutputMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccbaa3e1dfa33392b66a111cffdeb0df4527">CY_CTDAC_OUTPUT_HIGHZ</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Hold<sp />voltage.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__sample__hold_1ga08a0df606bea1204aa32ea5d343ac650">Cy_CTB_DACSampleAndHold</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga3f70d00ef0ce087244399f0784065502a04a0f0b544d3cc435b6aa30afaf3e2bd">CY_CTB_SH_HOLD</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1014" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="982" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1057" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This function enables sample and hold of the CTDAC output. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>