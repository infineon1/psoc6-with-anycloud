<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sar__functions__countsto" kind="group">
    <compoundname>group_sar_functions_countsto</compoundname>
    <title>Counts Conversion Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1ga5c070458f491389cab67bacb0beff85b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>int16_t</type>
        <definition>int16_t Cy_SAR_RawCounts2Counts</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int16_t adcCounts)</argsstring>
        <name>Cy_SAR_RawCounts2Counts</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int16_t</type>
          <declname>adcCounts</declname>
        </param>
        <briefdescription>
<para>Convert the channel result to a consistent result after accounting for averaging and subtracting the offset. </para>        </briefdescription>
        <detaileddescription>
<para>The equation used is: <verbatim>Counts = (RawCounts/AvgDivider - Offset)
</verbatim></para><para>where,<itemizedlist>
<listitem><para>RawCounts: Raw counts from SAR 16-bit CHAN_RESULT register</para></listitem><listitem><para>AvgDivider: divider based on averaging mode (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga9632ebe875d036e34a15d7c8df57e331">cy_en_sar_sample_ctrl_avg_mode_t</ref>) and number of samples averaged (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gaed912a3edfab12b4ebea94fedf289ecf">cy_en_sar_sample_ctrl_avg_cnt_t</ref>)<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a91a5fad5906a200940bbfbf15ef08868">CY_SAR_AVG_MODE_SEQUENTIAL_ACCUM</ref> : AvgDivider is the number of samples averaged or 16, whichever is smaller</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331ade22974fa0104308e9484cc606842b2d">CY_SAR_AVG_MODE_SEQUENTIAL_FIXED</ref> : AvgDivider is 1</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a89d83498d39e5e049cb2b33c18145095">CY_SAR_AVG_MODE_INTERLEAVED</ref> : AvgDivider is the number of samples averaged</para></listitem></itemizedlist>
</para></listitem><listitem><para>Offset: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1gacb47a32670fdb8439eeaafeed8cc36fb">Cy_SAR_SetChannelOffset</ref> function.</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>adcCounts</parametername>
</parameternamelist>
<parameterdescription>
<para>Conversion result from <ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>adcCounts after averaging and offset adjustments. If channel number is invalid, adcCounts is returned unmodified.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
This function is used by <ref kindref="member" refid="group__group__sar__functions__countsto_1gaf9e34e8b68020602396686ec699314a1">Cy_SAR_CountsTo_Volts</ref>, <ref kindref="member" refid="group__group__sar__functions__countsto_1ga8f1189056459a76c4b9f9917dc3a9e6e">Cy_SAR_CountsTo_mVolts</ref>, and <ref kindref="member" refid="group__group__sar__functions__countsto_1ga0950546e50604fc6daa33b2bdb155e5d">Cy_SAR_CountsTo_uVolts</ref>. Calling this function directly is usually not needed. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="976" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="929" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1427" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1gaf9e34e8b68020602396686ec699314a1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syslib__macros_1ga4611b605e45ab401f02cab15c5e38715">float32_t</ref></type>
        <definition>float32_t Cy_SAR_CountsTo_Volts</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int16_t adcCounts)</argsstring>
        <name>Cy_SAR_CountsTo_Volts</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int16_t</type>
          <declname>adcCounts</declname>
        </param>
        <briefdescription>
<para>Convert the ADC output to Volts as a float32. </para>        </briefdescription>
        <detaileddescription>
<para>For example, if the ADC measured 0.534 volts, the return value would be 0.534. The calculation of voltage depends on the channel offset, gain and other parameters. The equation used is: <verbatim>V = (RawCounts/AvgDivider - Offset)*TEN_VOLT/Gain
</verbatim></para><para>where,<itemizedlist>
<listitem><para>RawCounts: Raw counts from SAR 16-bit CHAN_RESULT register</para></listitem><listitem><para>AvgDivider: divider based on averaging mode (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga9632ebe875d036e34a15d7c8df57e331">cy_en_sar_sample_ctrl_avg_mode_t</ref>) and number of samples averaged (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gaed912a3edfab12b4ebea94fedf289ecf">cy_en_sar_sample_ctrl_avg_cnt_t</ref>)<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a91a5fad5906a200940bbfbf15ef08868">CY_SAR_AVG_MODE_SEQUENTIAL_ACCUM</ref> : AvgDivider is the number of samples averaged or 16, whichever is smaller</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331ade22974fa0104308e9484cc606842b2d">CY_SAR_AVG_MODE_SEQUENTIAL_FIXED</ref> : AvgDivider is 1</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a89d83498d39e5e049cb2b33c18145095">CY_SAR_AVG_MODE_INTERLEAVED</ref> : AvgDivider is the number of samples averaged</para></listitem></itemizedlist>
</para></listitem><listitem><para>Offset: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1gacb47a32670fdb8439eeaafeed8cc36fb">Cy_SAR_SetChannelOffset</ref> function.</para></listitem><listitem><para>TEN_VOLT: 10 V constant since the gain is in counts per 10 volts.</para></listitem><listitem><para>Gain: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1ga39cd1ae1f2f7101812199b012f22b635">Cy_SAR_SetChannelGain</ref> function.</para></listitem></itemizedlist>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This funtion is only valid when result alignment is right aligned.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>adcCounts</parametername>
</parameternamelist>
<parameterdescription>
<para>Conversion result from <ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Result in Volts.<itemizedlist>
<listitem><para>If channel number is invalid, 0 is returned.</para></listitem><listitem><para>If channel is left aligned, 0 is returned.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ADC<sp />conversion<sp />has<sp />completed<sp />and<sp />result<sp />is<sp />valid.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Retrieve<sp />the<sp />result<sp />on<sp />channel<sp />0<sp />and<sp />convert<sp />it<sp />to<sp />volts.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />chan<sp />=<sp />0UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syslib__macros_1ga4611b605e45ab401f02cab15c5e38715">float32_t</ref><sp />resultVolts;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />resultVolts<sp />=<sp /><ref kindref="member" refid="group__group__sar__functions__countsto_1gaf9e34e8b68020602396686ec699314a1">Cy_SAR_CountsTo_Volts</ref>(SAR,<sp />chan,<sp /><ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref>(SAR,<sp />chan));</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1041" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1023" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1428" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1ga8f1189056459a76c4b9f9917dc3a9e6e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>int16_t</type>
        <definition>int16_t Cy_SAR_CountsTo_mVolts</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int16_t adcCounts)</argsstring>
        <name>Cy_SAR_CountsTo_mVolts</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int16_t</type>
          <declname>adcCounts</declname>
        </param>
        <briefdescription>
<para>Convert the ADC output to millivolts as an int16. </para>        </briefdescription>
        <detaileddescription>
<para>For example, if the ADC measured 0.534 volts, the return value would be 534. The calculation of voltage depends on the channel offset, gain and other parameters. The equation used is: <verbatim>V = (RawCounts/AvgDivider - Offset)*TEN_VOLT/Gain
mV = V * 1000
</verbatim></para><para>where,<itemizedlist>
<listitem><para>RawCounts: Raw counts from SAR 16-bit CHAN_RESULT register</para></listitem><listitem><para>AvgDivider: divider based on averaging mode (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga9632ebe875d036e34a15d7c8df57e331">cy_en_sar_sample_ctrl_avg_mode_t</ref>) and number of samples averaged (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gaed912a3edfab12b4ebea94fedf289ecf">cy_en_sar_sample_ctrl_avg_cnt_t</ref>)<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a91a5fad5906a200940bbfbf15ef08868">CY_SAR_AVG_MODE_SEQUENTIAL_ACCUM</ref> : AvgDivider is the number of samples averaged or 16, whichever is smaller</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331ade22974fa0104308e9484cc606842b2d">CY_SAR_AVG_MODE_SEQUENTIAL_FIXED</ref> : AvgDivider is 1</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a89d83498d39e5e049cb2b33c18145095">CY_SAR_AVG_MODE_INTERLEAVED</ref> : AvgDivider is the number of samples averaged</para></listitem></itemizedlist>
</para></listitem><listitem><para>Offset: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1gacb47a32670fdb8439eeaafeed8cc36fb">Cy_SAR_SetChannelOffset</ref> function.</para></listitem><listitem><para>TEN_VOLT: 10 V constant since the gain is in counts per 10 volts.</para></listitem><listitem><para>Gain: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1ga39cd1ae1f2f7101812199b012f22b635">Cy_SAR_SetChannelGain</ref> function.</para></listitem></itemizedlist>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This funtion is only valid when result alignment is right aligned.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>adcCounts</parametername>
</parameternamelist>
<parameterdescription>
<para>Conversion result from <ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Result in millivolts.<itemizedlist>
<listitem><para>If channel number is invalid, 0 is returned.</para></listitem><listitem><para>If channel is left aligned, 0 is returned.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ADC<sp />conversion<sp />has<sp />completed<sp />and<sp />result<sp />is<sp />valid.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Retrieve<sp />the<sp />result<sp />on<sp />channel<sp />0<sp />and<sp />convert<sp />it<sp />to<sp />millivolts.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />chan<sp />=<sp />0UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />int16_t<sp />resultmVolts;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />resultmVolts<sp />=<sp /><ref kindref="member" refid="group__group__sar__functions__countsto_1ga8f1189056459a76c4b9f9917dc3a9e6e">Cy_SAR_CountsTo_mVolts</ref>(SAR,<sp />chan,<sp /><ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref>(SAR,<sp />chan));</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1115" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1089" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1429" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1ga0950546e50604fc6daa33b2bdb155e5d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>int32_t</type>
        <definition>int32_t Cy_SAR_CountsTo_uVolts</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int16_t adcCounts)</argsstring>
        <name>Cy_SAR_CountsTo_uVolts</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int16_t</type>
          <declname>adcCounts</declname>
        </param>
        <briefdescription>
<para>Convert the ADC output to microvolts as a int32. </para>        </briefdescription>
        <detaileddescription>
<para>For example, if the ADC measured 0.534 volts, the return value would be 534000. The calculation of voltage depends on the channel offset, gain and other parameters. The equation used is: <verbatim>V = (RawCounts/AvgDivider - Offset)*TEN_VOLT/Gain
uV = V * 1000000
</verbatim></para><para>where,<itemizedlist>
<listitem><para>RawCounts: Raw counts from SAR 16-bit CHAN_RESULT register</para></listitem><listitem><para>AvgDivider: divider based on averaging mode (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga9632ebe875d036e34a15d7c8df57e331">cy_en_sar_sample_ctrl_avg_mode_t</ref>) and number of samples averaged (<ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gaed912a3edfab12b4ebea94fedf289ecf">cy_en_sar_sample_ctrl_avg_cnt_t</ref>)<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a91a5fad5906a200940bbfbf15ef08868">CY_SAR_AVG_MODE_SEQUENTIAL_ACCUM</ref> : AvgDivider is the number of samples averaged or 16, whichever is smaller</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331ade22974fa0104308e9484cc606842b2d">CY_SAR_AVG_MODE_SEQUENTIAL_FIXED</ref> : AvgDivider is 1</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga9632ebe875d036e34a15d7c8df57e331a89d83498d39e5e049cb2b33c18145095">CY_SAR_AVG_MODE_INTERLEAVED</ref> : AvgDivider is the number of samples averaged</para></listitem></itemizedlist>
</para></listitem><listitem><para>Offset: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1gacb47a32670fdb8439eeaafeed8cc36fb">Cy_SAR_SetChannelOffset</ref> function.</para></listitem><listitem><para>TEN_VOLT: 10 V constant since the gain is in counts per 10 volts.</para></listitem><listitem><para>Gain: Value stored by the <ref kindref="member" refid="group__group__sar__functions__countsto_1ga39cd1ae1f2f7101812199b012f22b635">Cy_SAR_SetChannelGain</ref> function.</para></listitem></itemizedlist>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This funtion is only valid when result alignment is right aligned.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>adcCounts</parametername>
</parameternamelist>
<parameterdescription>
<para>Conversion result from <ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Result in microvolts.<itemizedlist>
<listitem><para>If channel number is valid, 0 is returned.</para></listitem><listitem><para>If channel is left aligned, 0 is returned.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ADC<sp />conversion<sp />has<sp />completed<sp />and<sp />result<sp />is<sp />valid.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Retrieve<sp />the<sp />result<sp />on<sp />channel<sp />0<sp />and<sp />convert<sp />it<sp />to<sp />microvolts.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />chan<sp />=<sp />0UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />int32_t<sp />resultuVolts;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />resultuVolts<sp />=<sp /><ref kindref="member" refid="group__group__sar__functions__countsto_1ga0950546e50604fc6daa33b2bdb155e5d">Cy_SAR_CountsTo_uVolts</ref>(SAR,<sp />chan,<sp /><ref kindref="member" refid="group__group__sar__functions__basic_1ga5d7a3489629382b1e46477c034bbb2d0">Cy_SAR_GetResult16</ref>(SAR,<sp />chan));</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1180" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1163" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1430" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1gacb47a32670fdb8439eeaafeed8cc36fb" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sar__enums_1ga4061024a91b0f2e3aca4716fce722b57">cy_en_sar_status_t</ref></type>
        <definition>cy_en_sar_status_t Cy_SAR_SetChannelOffset</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int16_t offset)</argsstring>
        <name>Cy_SAR_SetChannelOffset</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int16_t</type>
          <declname>offset</declname>
        </param>
        <briefdescription>
<para>Store the channel offset for the voltage conversion functions. </para>        </briefdescription>
        <detaileddescription>
<para>Offset is applied to counts before unit scaling and gain. See <ref kindref="member" refid="group__group__sar__functions__countsto_1gaf9e34e8b68020602396686ec699314a1">Cy_SAR_CountsTo_Volts</ref> for more about this formula.</para><para>To change channel 0's offset based on a known V_offset_mV, use: <verbatim>Cy_SAR_SetOffset(0UL, -1 * V_offset_mV * (1UL &lt;&lt; Resolution) / (2 * V_ref_mV));
</verbatim></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>The count value measured when the inputs are shorted or connected to the same input voltage.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__enums_1gga4061024a91b0f2e3aca4716fce722b57a1bcc04151b677cb6e16050b40309b21a">CY_SAR_SUCCESS</ref> : offset was set successfully</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__enums_1gga4061024a91b0f2e3aca4716fce722b57a57b9a5eff2273406f83163b1327d90a3">CY_SAR_BAD_PARAM</ref> : channel number is equal to or greater than <ref kindref="member" refid="group__group__sar__macros_1ga018e45aa16796de387bf76bb146070b6">CY_SAR_NUM_CHANNELS</ref> </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="842" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="829" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1431" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__countsto_1ga39cd1ae1f2f7101812199b012f22b635" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sar__enums_1ga4061024a91b0f2e3aca4716fce722b57">cy_en_sar_status_t</ref></type>
        <definition>cy_en_sar_status_t Cy_SAR_SetChannelGain</definition>
        <argsstring>(const SAR_Type *base, uint32_t chan, int32_t adcGain)</argsstring>
        <name>Cy_SAR_SetChannelGain</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>chan</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>adcGain</declname>
        </param>
        <briefdescription>
<para>Store the gain value for the voltage conversion functions. </para>        </briefdescription>
        <detaileddescription>
<para>The gain is configured at initialization in <ref kindref="member" refid="group__group__sar__functions__basic_1ga5c053a77ca29519ca29da56da7eaeebe">Cy_SAR_Init</ref> based on the SARADC resolution and voltage reference.</para><para>Gain is applied after offset and unit scaling. See <ref kindref="member" refid="group__group__sar__functions__countsto_1gaf9e34e8b68020602396686ec699314a1">Cy_SAR_CountsTo_Volts</ref> for more about this formula.</para><para>To change channel 0's gain based on a known V_ref_mV, use: <verbatim>Cy_SAR_SetGain(0UL, 10000 * (1UL &lt;&lt; Resolution) / (2 * V_ref_mV));
</verbatim></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>chan</parametername>
</parameternamelist>
<parameterdescription>
<para>The channel number, between 0 and <ref kindref="member" refid="group__group__sar__macros_1ga92cec01d80f386f95916cc4f0ef238af">CY_SAR_INJ_CHANNEL</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>adcGain</parametername>
</parameternamelist>
<parameterdescription>
<para>The gain in counts per 10 volt.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sar__enums_1gga4061024a91b0f2e3aca4716fce722b57a1bcc04151b677cb6e16050b40309b21a">CY_SAR_SUCCESS</ref> : gain was set successfully</para></listitem><listitem><para><ref kindref="member" refid="group__group__sar__enums_1gga4061024a91b0f2e3aca4716fce722b57a57b9a5eff2273406f83163b1327d90a3">CY_SAR_BAD_PARAM</ref> : channel number is equal to or greater than <ref kindref="member" refid="group__group__sar__macros_1ga018e45aa16796de387bf76bb146070b6">CY_SAR_NUM_CHANNELS</ref> </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="887" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="874" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1432" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions performs counts to *volts conversions. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>