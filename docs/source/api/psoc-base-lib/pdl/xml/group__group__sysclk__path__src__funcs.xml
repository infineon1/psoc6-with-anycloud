<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__path__src__funcs" kind="group">
    <compoundname>group_sysclk_path_src_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__path__src__funcs_1ga517f603266062d0013947ea950ed5b60" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sysclk__returns_1gad6699a184e2e3c01433251b0981558f3">cy_en_sysclk_status_t</ref></type>
        <definition>cy_en_sysclk_status_t Cy_SysClk_ClkPathSetSource</definition>
        <argsstring>(uint32_t clkPath, cy_en_clkpath_in_sources_t source)</argsstring>
        <name>Cy_SysClk_ClkPathSetSource</name>
        <param>
          <type>uint32_t</type>
          <declname>clkPath</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sysclk__path__src__enums_1ga8ddaf9023a02dee0d1f9a5629d6ccfe6">cy_en_clkpath_in_sources_t</ref></type>
          <declname>source</declname>
        </param>
        <briefdescription>
<para>Configures the source for the specified clock path. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>clkPath</parametername>
</parameternamelist>
<parameterdescription>
<para>Selects which clock path to configure; 0 is the first clock path, which is the FLL.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>source</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__sysclk__path__src__enums_1ga8ddaf9023a02dee0d1f9a5629d6ccfe6">cy_en_clkpath_in_sources_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sysclk__returns_1gad6699a184e2e3c01433251b0981558f3">cy_en_sysclk_status_t</ref> CY_SYSCLK_INVALID_STATE - ECO already enabled For the PSoC 64 devices there are possible situations when function returns the PRA error status code. This is because for PSoC 64 devices the function uses the PRA driver to change the protected registers. Refer to <ref kindref="member" refid="group__group__pra__enums_1ga60be13e12e82986f8c0d6c6a6d4f12c5">cy_en_pra_status_t</ref> for more details.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If calling this function changes an FLL or PLL input frequency, disable the FLL or PLL before calling this function. After calling this function, call the FLL or PLL configure function, for example &lt;a href="group__group__sysclk__fll__funcs.html#group__group__sysclk__fll__funcs_1gad9d9c36d022475746375bddaba2b2065"&gt;Cy_SysClk_FllConfigure()&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call &lt;a href="group__group__system__config__system__functions.html#group__group__system__config__system__functions_1gae0c36a9591fe6e9c45ecb21a794f0f0f"&gt;SystemCoreClockUpdate&lt;/a&gt; after this function calling if it affects the CLK_HF0 frequency.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call &lt;a href="group__group__syslib__functions.html#group__group__syslib__functions_1ga8b897f8554957f9393f645d5ab1106c9"&gt;Cy_SysLib_SetWaitStates&lt;/a&gt; before calling this function if it affects the CLK_HF0 frequency and the frequency is increasing.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call &lt;a href="group__group__syslib__functions.html#group__group__syslib__functions_1ga8b897f8554957f9393f645d5ab1106c9"&gt;Cy_SysLib_SetWaitStates&lt;/a&gt; after calling this function if it affects the CLK_HF0 frequency and the frequency is decreasing.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ECO<sp />needs<sp />to<sp />source<sp />HFCLK2<sp />through<sp />Path<sp />2.<sp />The<sp />ECO<sp />is</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />configured<sp />through<sp />its<sp />function<sp />calls.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CLKPATH2<sp />(2UL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />HFCLK2<sp />(2UL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPathMuxFreq<sp />=<sp />0UL;<sp /></highlight><highlight class="comment">/*<sp />Variable<sp />to<sp />store<sp />the<sp />Clock<sp />Path<sp />Mux<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />clock<sp />path<sp />2<sp />mux<sp />to<sp />be<sp />sourced<sp />from<sp />ECO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__sysclk__returns_1ggad6699a184e2e3c01433251b0981558f3a1563f761f963757b339ff05eb5a690ec">CY_SYSCLK_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1ga517f603266062d0013947ea950ed5b60">Cy_SysClk_ClkPathSetSource</ref>(CLKPATH2,<sp /><ref kindref="member" refid="group__group__sysclk__path__src__enums_1gga8ddaf9023a02dee0d1f9a5629d6ccfe6a28f5ad80fd20d8459d40aa8b86d3a77e">CY_SYSCLK_CLKPATH_IN_ECO</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clkPathMuxFreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1ga1256c8a7290cecf2b9553a1df582e797">Cy_SysClk_ClkPathMuxGetFrequency</ref>(CLKPATH2);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />clkPathMuxFreq<sp />contains<sp />the<sp />Clock<sp />Path<sp />Mux<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Perform<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />HFCLK2<sp />source<sp />to<sp />clock<sp />path<sp />2<sp />and<sp />enable<sp />HFCLK2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__clk__hf__funcs_1ga4d090b2e9716939fe4689fac77b05679">Cy_SysClk_ClkHfSetSource</ref>(HFCLK2,<sp /><ref kindref="member" refid="group__group__sysclk__clk__hf__enums_1ggabac2d6b9124a00860dcd781a922788d6a7a35a9d5b37d1add71bd6f866953366e">CY_SYSCLK_CLKHF_IN_CLKPATH2</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />HFCLK2<sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__clk__hf__funcs_1gaaa461cfa69f6d1b94536d4fbdc73fae9">Cy_SysClk_ClkHfEnable</ref>(HFCLK2);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="430" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sysclk.c" bodystart="402" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="877" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__path__src__funcs_1gab1c9f683f870696d41786c3df1eb331b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sysclk__path__src__enums_1ga8ddaf9023a02dee0d1f9a5629d6ccfe6">cy_en_clkpath_in_sources_t</ref></type>
        <definition>cy_en_clkpath_in_sources_t Cy_SysClk_ClkPathGetSource</definition>
        <argsstring>(uint32_t clkPath)</argsstring>
        <name>Cy_SysClk_ClkPathGetSource</name>
        <param>
          <type>uint32_t</type>
          <declname>clkPath</declname>
        </param>
        <briefdescription>
<para>Reports which source is selected for the path mux. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>clkPath</parametername>
</parameternamelist>
<parameterdescription>
<para>Selects which clock path to report; 0 is the first clock path, which is the FLL.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sysclk__path__src__enums_1ga8ddaf9023a02dee0d1f9a5629d6ccfe6">cy_en_clkpath_in_sources_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />A<sp />peripheral<sp />derived<sp />off<sp />of<sp />Path<sp />1<sp />clock<sp />is<sp />not<sp />clocking<sp />at<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />expected<sp />frequency<sp />and<sp />accuracy.<sp />Need<sp />to<sp />confirm<sp />that<sp />the<sp />source</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />the<sp />Path<sp />1<sp />mux<sp />is<sp />the<sp />ECO.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />source<sp />of<sp />the<sp />clock<sp />path<sp />1<sp />mux<sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__sysclk__path__src__enums_1gga8ddaf9023a02dee0d1f9a5629d6ccfe6a28f5ad80fd20d8459d40aa8b86d3a77e">CY_SYSCLK_CLKPATH_IN_ECO</ref><sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1gab1c9f683f870696d41786c3df1eb331b">Cy_SysClk_ClkPathGetSource</ref>(1UL))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="458" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sysclk.c" bodystart="447" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="878" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__path__src__funcs_1ga1256c8a7290cecf2b9553a1df582e797" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SysClk_ClkPathMuxGetFrequency</definition>
        <argsstring>(uint32_t clkPath)</argsstring>
        <name>Cy_SysClk_ClkPathMuxGetFrequency</name>
        <param>
          <type>uint32_t</type>
          <declname>clkPath</declname>
        </param>
        <briefdescription>
<para>Returns the output frequency of the clock path mux. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The output frequency of the path mux.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If the return value equals zero, that means either:&lt;ul&gt;&lt;li&gt;&lt;p&gt;the selected path mux source signal frequency is unknown (e.g. dsi_out, etc.) or&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;the selected path mux source is not configured/enabled/stable (e.g. ECO, EXTCLK, etc.).&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ECO<sp />needs<sp />to<sp />source<sp />HFCLK2<sp />through<sp />Path<sp />2.<sp />The<sp />ECO<sp />is</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />configured<sp />through<sp />its<sp />function<sp />calls.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CLKPATH2<sp />(2UL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />HFCLK2<sp />(2UL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPathMuxFreq<sp />=<sp />0UL;<sp /></highlight><highlight class="comment">/*<sp />Variable<sp />to<sp />store<sp />the<sp />Clock<sp />Path<sp />Mux<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />clock<sp />path<sp />2<sp />mux<sp />to<sp />be<sp />sourced<sp />from<sp />ECO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__sysclk__returns_1ggad6699a184e2e3c01433251b0981558f3a1563f761f963757b339ff05eb5a690ec">CY_SYSCLK_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1ga517f603266062d0013947ea950ed5b60">Cy_SysClk_ClkPathSetSource</ref>(CLKPATH2,<sp /><ref kindref="member" refid="group__group__sysclk__path__src__enums_1gga8ddaf9023a02dee0d1f9a5629d6ccfe6a28f5ad80fd20d8459d40aa8b86d3a77e">CY_SYSCLK_CLKPATH_IN_ECO</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clkPathMuxFreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1ga1256c8a7290cecf2b9553a1df582e797">Cy_SysClk_ClkPathMuxGetFrequency</ref>(CLKPATH2);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />clkPathMuxFreq<sp />contains<sp />the<sp />Clock<sp />Path<sp />Mux<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Perform<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />HFCLK2<sp />source<sp />to<sp />clock<sp />path<sp />2<sp />and<sp />enable<sp />HFCLK2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__clk__hf__funcs_1ga4d090b2e9716939fe4689fac77b05679">Cy_SysClk_ClkHfSetSource</ref>(HFCLK2,<sp /><ref kindref="member" refid="group__group__sysclk__clk__hf__enums_1ggabac2d6b9124a00860dcd781a922788d6a7a35a9d5b37d1add71bd6f866953366e">CY_SYSCLK_CLKHF_IN_CLKPATH2</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />HFCLK2<sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__clk__hf__funcs_1gaaa461cfa69f6d1b94536d4fbdc73fae9">Cy_SysClk_ClkHfEnable</ref>(HFCLK2);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="524" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sysclk.c" bodystart="477" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="879" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__path__src__funcs_1ga54c618c89782d227fb8f292d1dc15625" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SysClk_ClkPathGetFrequency</definition>
        <argsstring>(uint32_t clkPath)</argsstring>
        <name>Cy_SysClk_ClkPathGetFrequency</name>
        <param>
          <type>uint32_t</type>
          <declname>clkPath</declname>
        </param>
        <briefdescription>
<para>Returns the output frequency of the clock path mux. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The output frequency of the path mux.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If the return value equals zero, that means either:&lt;ul&gt;&lt;li&gt;&lt;p&gt;the selected path mux source signal frequency is unknown (e.g. dsi_out, etc.) or&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;the selected path mux source is not configured/enabled/stable (e.g. ECO, EXTCLK, etc.).&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />FLL<sp />is<sp />configured<sp />and<sp />needs<sp />to<sp />be<sp />enabled<sp />within<sp />2<sp />ms<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CLKPATH0<sp />(0UL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPathFreq<sp />=<sp />0UL;<sp /></highlight><highlight class="comment">/*<sp />Variable<sp />to<sp />store<sp />the<sp />Clock<sp />Path<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />FLL<sp />with<sp />a<sp />timeout<sp />of<sp />2000<sp />microseconds<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__sysclk__returns_1ggad6699a184e2e3c01433251b0981558f3a1563f761f963757b339ff05eb5a690ec">CY_SYSCLK_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__sysclk__fll__funcs_1ga5a008909d3f50d85fb8d8c9f56ed8886">Cy_SysClk_FllEnable</ref>(2000UL))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clkPathFreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__path__src__funcs_1ga54c618c89782d227fb8f292d1dc15625">Cy_SysClk_ClkPathGetFrequency</ref>(CLKPATH0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />clkPathFreq<sp />contains<sp />an<sp />actual<sp />FLL<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="584" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sysclk.c" bodystart="543" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="880" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>