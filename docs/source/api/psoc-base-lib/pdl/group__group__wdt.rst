WDT (Watchdog Timer)
=====================


.. doxygengroup:: group_wdt
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   
   group__group__wdt__macros.rst
   group__group__wdt__functions.rst

