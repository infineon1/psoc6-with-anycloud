=========================
Initialization Functions
=========================

.. doxygengroup:: group_gpio_functions_init
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
  