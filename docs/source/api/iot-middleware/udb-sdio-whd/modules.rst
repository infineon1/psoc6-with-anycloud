===============
API Reference
===============

The following provides a list of API documentation


+-----------------------------------+-----------------------------------+
| `Pin                              |                                   |
| States <group__group__bsp__pin__s |                                   |
| tate.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
| `Pin                              |                                   |
| Mappings <group__group__bsp__pins |                                   |
| .html>`__                         |                                   |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__bsp__ma    |                                   |
| cros.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__bsp_    |                                   |
| _functions.html>`__               |                                   |
+-----------------------------------+-----------------------------------+
| `UDB_SDIO <group__group__udb_     | SDIO - Secure Digital Input       |
| _sdio.html>`__                    | Output is a standard for          |
|                                   | communicating with various        |
|                                   | external devices such as Wifi and |
|                                   | bluetooth devices                 |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__udb__sd    |                                   |
| io__macros.html>`__               |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__udb_    |                                   |
| _sdio__functions.html>`__         |                                   |
+-----------------------------------+-----------------------------------+
| `Data                             |                                   |
| Structures <group__group__udb__sd |                                   |
| io__data__structures.html>`__     |                                   |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst
   group__group__udb__sdio.rst




