==================
Device Management
==================


.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__btm__ble__api__functions.rst
   group__wicedbt__bredr.rst
   group__wicedbt__utility.rst
   
   
.. doxygengroup:: wicedbt_DeviceManagement
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
