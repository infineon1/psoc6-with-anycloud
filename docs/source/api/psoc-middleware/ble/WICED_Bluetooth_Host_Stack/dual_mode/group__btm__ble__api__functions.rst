==========================
BLE (Bluetooth Low Energy)
==========================

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__btm__ble__adv__scan__functions.rst
   group__btm__ble__conn__whitelist__functions.rst
   group__btm__ble__phy__functions.rst
   group__btm__ble__multi__adv__functions.rst
   group__btm__ble__sec__api__functions.rst
   
   
.. doxygengroup:: btm_ble_api_functions
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
