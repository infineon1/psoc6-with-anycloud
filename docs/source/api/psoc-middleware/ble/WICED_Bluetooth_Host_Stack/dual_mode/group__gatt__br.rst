===================================================
BR/EDR (Bluetooth Basic Rate / Enhanced Data Rate)
===================================================

.. doxygengroup:: gatt_br
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: