=====================
MPEG-2,4 AAC Support
=====================

.. doxygengroup:: wicedbt_a2dp_mpeg_2_4
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: