=================
Device Management
=================

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   
   group__btm__ble__adv__scan__functions.rst
   group__btm__ble__conn__whitelist__functions.rst
   group__btm__ble__phy__functions.rst
   group__btm__ble__multi__adv__functions.rst
   group__wicedbt__utility.rst
   group__btm__ble__sec__api__functions.rst


.. doxygengroup:: Wicedbt_DeviceManagement
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
