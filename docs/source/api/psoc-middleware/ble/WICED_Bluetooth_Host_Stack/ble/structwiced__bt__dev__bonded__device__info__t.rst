==========================================
wiced_bt_dev_bonded_device_info_t struct
==========================================

.. doxygenstruct:: wiced_bt_dev_bonded_device_info_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: