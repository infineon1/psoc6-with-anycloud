===============
Wiced BT Types
===============

.. doxygengroup:: bt_types
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: