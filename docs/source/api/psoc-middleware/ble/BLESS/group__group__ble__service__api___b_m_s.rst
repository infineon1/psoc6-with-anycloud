==============================
Bond Management Service (BMS)
==============================

.. doxygengroup:: group_ble_service_api_BMS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___b_m_s__server__client.rst
   group__group__ble__service__api___b_m_s__server.rst
   group__group__ble__service__api___b_m_s__client.rst
   group__group__ble__service__api___b_m_s__definitions.rst