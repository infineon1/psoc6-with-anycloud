=========
CapSense
=========

.. raw:: html

   <script type="text/javascript">
   window.location.href = "ModusToolbox_CapSense_Configurator_Guide.html"
   </script>

.. toctree::
   :hidden:

   ModusToolbox_CapSense_Configurator_Guide.rst
   ModusToolbox_CapSense_Tuner_Guide.rst
   CapSense Middleware/CapSense Middleware.rst
   CapSense_Tuner_Client_over_BLE_Example.rst
   CapSense_Tuner_Server_over_BLE_Example.rst
   CapSense_Buttons_and_Slider_Example.rst
   CapSenes_FreeRTOS_Example.rst
   CapSense_Buttons_BLE_Example.rst