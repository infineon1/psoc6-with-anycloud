=================
Enumerated types
=================
   
.. doxygengroup:: group_csdadc_enums
   :project: csdadc
   :members:
   :protected-members:
   :private-members:
   :undoc-members: