===============================================
AES Encryption/Decryption Example
===============================================

This code example encrypts and decrypts user input data using the AES algorithm with a 128-bit key. The encrypted and decrypted data are displayed on a UART terminal emulator. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-psoc6-crypto-aes" target="_blank">Click here to be taken to the project on GitHub.</a><br><br>
